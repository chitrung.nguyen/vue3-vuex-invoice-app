import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBPeXBnDbPMo5eOFpMjgpDGAr9xhEzSTsk",
  authDomain: "invoice-vue3-app.firebaseapp.com",
  projectId: "invoice-vue3-app",
  storageBucket: "invoice-vue3-app.appspot.com",
  messagingSenderId: "63586706989",
  appId: "1:63586706989:web:157f014a00cee8176abaa4"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();

